import argparse
import enum
import json
import logging
import pickle
import os
import typing
from urllib import parse as url_parse

import frozendict  # type: ignore
import pandas  # type: ignore

from d3m import container, exceptions, types
from d3m.metadata import base as metadata_base, hyperparams as hyperparams_module, pipeline as pipeline_module, problem
from d3m.primitive_interfaces import base

logger = logging.getLogger(__name__)


class Phase(enum.IntEnum):
    FIT = 1
    PRODUCE = 2


# TODO: Add debug logging to the runtime.
class Runtime:
    """
    Reference runtime to fit and produce a pipeline.

    Parameters
    ----------
    pipeline : Pipeline
        A pipeline to run.
    hyperparams : Sequence
        Values for free hyper-parameters of the pipeline. It should be a list, where each element corresponds
        to free hyper-parameters of the corresponding pipeline step. Not all free hyper-parameters have to be
        specified. Default values are used for those which are not. Optional.
    problem_description : Dict
        A parsed problem description in standard problem description schema.
    random_seed : int
        A random seed to use for this run. This control all randomness during the run.
    volumes_dir : str
        Path to a directory with static files required by primitives.
        In the standard directory structure (as obtained running ``python3 -m d3m.index download``).
    is_standard_pipeline : bool
        Is the pipeline a standard pipeline?

    Attributes
    ----------
    pipeline : Pipeline
        A pipeline to run.
    hyperparams : Sequence
        Values for free hyper-parameters of the pipeline. It should be a list, where each element corresponds
        to free hyper-parameters of the corresponding pipeline step. Not all free hyper-parameters have to be
        specified. Default values are used for those which are not. Optional.
    problem_description : Dict
        A parsed problem description in standard problem description schema.
    random_seed : int
        A random seed to use for this run. This control all randomness during the run.
    volumes_dir : str
        Path to a directory with static files required by primitives.
        In the standard directory structure (as obtained running ``python3 -m d3m.index download``).
    is_standard_pipeline : bool
        Is the pipeline a standard pipeline?
    current_step : int
        Which step is currently being ran.
    phase : Phase
        Which phase are we currently running.
    environment : Dict[str, Any]
        Map between available data references and their values during the run.
    steps_state : List[Union[PrimitiveBase, Runtime]]
        Fitted state for each step of the pipeline.
    """

    def __init__(self, pipeline: pipeline_module.Pipeline, hyperparams: typing.Sequence = None, *,
                 problem_description: typing.Dict = None, random_seed: int = 0, volumes_dir: str = None,
                 is_standard_pipeline: bool = False) -> None:
        self.pipeline = pipeline
        self.hyperparams = hyperparams
        self.problem_description = problem_description
        self.random_seed = random_seed
        self.volumes_dir = volumes_dir
        self.is_standard_pipeline = is_standard_pipeline

        # Preliminary check.
        self.pipeline.check(allow_placeholders=False, standard_pipeline=self.is_standard_pipeline)

        if self.hyperparams is not None:
            # Check hyper-parameters.
            self._check_hyperparams(self.pipeline, self.hyperparams)

        self.steps_state: typing.List[typing.Union[base.PrimitiveBase, Runtime, None]] = [None for step in self.pipeline.steps]

        self._initialize_run_state([], None)

    def _initialize_run_state(self, inputs: typing.Sequence[typing.Any], phase: typing.Optional[Phase]) -> None:
        self.current_step = 0
        self.phase = phase
        # TODO: Remove values from the "environment" once they are not needed anymore to optimize memory use.
        self.environment: typing.Dict[str, typing.Any] = {}

        for i, input_value in enumerate(inputs):
            if isinstance(input_value, container.Dataset):
                input_value = self._mark_targets(input_value)

            self.environment['inputs.{i}'.format(i=i)] = input_value

    def _delete_run_state(self) -> None:
        """
        After a pipeline run, we delete state which was necessary while pipeline was running, but it is not needed anymore.
        """

        # We keep "steps_state" so that we can produce.

        self.current_step = 0
        self.phase = None
        self.environment = {}

    def __getstate__(self) -> dict:
        state = self.__dict__.copy()

        # We do not want to pickle run state.
        state['current_step'] = 0
        state['phase'] = None
        state['environment'] = {}

        return state

    def _check_hyperparams(self, pipeline: pipeline_module.Pipeline, hyperparams: typing.Sequence) -> None:
        """
        Check provided values for free hyper-parameters.
        """

        if not isinstance(hyperparams, typing.Sequence):
            raise exceptions.InvalidArgumentTypeError("Hyper-parameter values for the pipeline '{pipeline_id}' is not a sequence.".format(
                pipeline_id=pipeline.id,
            ))

        if len(hyperparams) != len(pipeline.steps):
            raise exceptions.InvalidArgumentValueError(
                "Hyper-parameter values for the pipeline '{pipeline_id}' do not match the number of steps in the pipeline: {hyperparams_steps} vs. {pipeline_steps}".format(
                    pipeline_id=pipeline.id,
                    hyperparams_steps=len(hyperparams),
                    pipeline_steps=len(pipeline.steps),
                ),
            )

        for step_index, (hyperparams_for_step, step) in enumerate(zip(hyperparams, pipeline.steps)):
            # Placeholder step is not really allowed, but we have it here for completeness.
            # Its "get_free_hyperparams" returns an empty list.
            if isinstance(step, pipeline_module.PlaceholderStep):
                if not isinstance(hyperparams_for_step, typing.Sequence):
                    raise exceptions.InvalidArgumentTypeError("Hyper-parameter values for placeholder step {step_index} of pipeline '{pipeline_id}' is not a sequence.".format(
                        step_index=step_index,
                        pipeline_id=pipeline.id,
                    ))

            elif isinstance(step, pipeline_module.SubpipelineStep):
                self._check_hyperparams(step.pipeline, hyperparams_for_step)

            elif isinstance(step, pipeline_module.PrimitiveStep):
                if not isinstance(hyperparams_for_step, (dict, frozendict.frozendict)):
                    raise exceptions.InvalidArgumentTypeError("Hyper-parameter values for primitive step {step_index} of pipeline '{pipeline_id}' is not a dict.".format(
                        step_index=step_index,
                        pipeline_id=pipeline.id,
                    ))

                hyperparams_for_step_keys = set(hyperparams_for_step.keys())
                free_hyperparams_keys = set(step.get_free_hyperparams().keys())

                if hyperparams_for_step_keys - free_hyperparams_keys:
                    raise exceptions.InvalidArgumentValueError(
                        "Hyper-parameter values for primitive step {step_index} of pipeline '{pipeline_id}' are overriding hyper-parameters fixed in the pipeline: {hyperparams}".format(
                            step_index=step_index,
                            pipeline_id=pipeline.id,
                            hyperparams=list(hyperparams_for_step_keys - free_hyperparams_keys),
                        ),
                    )

    def _check_pipeline(self, inputs: typing.Sequence[typing.Any]) -> None:
        """
        Check with known inputs.
        """

        input_types = {}
        for i, input_value in enumerate(inputs):
            input_types['inputs.{i}'.format(i=i)] = type(input_value)

        self.pipeline.check(allow_placeholders=False, standard_pipeline=self.is_standard_pipeline, input_types=input_types)

    def _run_placeholder(self, step: pipeline_module.PlaceholderStep) -> None:
        raise exceptions.InvalidPipelineError("Step {step_index} of pipeline '{pipeline_id}' is a placeholder but there should be no placeholders.".format(
            step_index=self.current_step,
            pipeline_id=self.pipeline.id,
        ))

    # TODO: Make return type be equal to the current's class type, so that it adapts if this class is subclassed.
    def _create_subpipeline(self, pipeline: pipeline_module.Pipeline, hyperparams: typing.Optional[typing.Sequence]) -> 'Runtime':
        """
        Creates an instance of the subpipeline's runtime.
        """

        # We change the random seed in a deterministic way so that it does not matter in which order we run steps.
        # Subpipelines are generally not a standard pipeline.
        return type(self)(pipeline, hyperparams, random_seed=self.random_seed + self.current_step, volumes_dir=self.volumes_dir)

    def _run_subpipeline(self, step: pipeline_module.SubpipelineStep) -> None:
        if step.pipeline is None:
            raise exceptions.InvalidPipelineError("Pipeline has not been resolved.")

        subpipeline_inputs: typing.List[typing.Any] = []
        for i, data_reference in enumerate(step.inputs):
            subpipeline_inputs.append(self.environment[data_reference])

        if self.hyperparams is not None:
            hyperparams = self.hyperparams[self.current_step]

            # We checked this already in "_check_hyperparams".
            assert isinstance(hyperparams, typing.Sequence), hyperparams
        else:
            hyperparams = None

        subpipeline = self._create_subpipeline(step.pipeline, hyperparams)

        assert self.steps_state[self.current_step] is None
        self.steps_state[self.current_step] = subpipeline

        outputs = subpipeline._run(subpipeline_inputs, self.phase)

        for i, output_id in enumerate(step.outputs):
            # "output_id" can be "None" if this output is not used and should be skipped.
            if output_id is not None:
                output_data_reference = 'steps.{i}.{output_id}'.format(i=step.index, output_id=output_id)

                self.environment[output_data_reference] = outputs[i]

    def _get_singleton_value(self, value: typing.Any, is_argument: bool, name: str) -> typing.Any:
        """
        A helper to extract a value from a singleton value (extracting a sole element of a
        container of length 1).
        """

        if isinstance(value, pandas.DataFrame):
            if value.shape[0] != 1:
                if is_argument:
                    raise exceptions.InvalidPipelineError(
                        "Argument '{argument_name}' of step {step_index} of pipeline '{pipeline_id}' is singleton data, but available data is not.".format(
                            argument_name=name,
                            step_index=self.current_step,
                            pipeline_id=self.pipeline.id,
                        ),
                    )
                else:
                    raise exceptions.InvalidPipelineError(
                        "Hyper-parameter '{hyperparameter_name}' of step {step_index} of pipeline '{pipeline_id}' is singleton data, but available data is not.".format(
                            hyperparameter_name=name,
                            step_index=self.current_step,
                            pipeline_id=self.pipeline.id,
                        ),
                    )

            # Fetch the row as a list. This assures different columns can be of a different type.
            singleton_value = container.List([value.iloc[0, k] for k in range(len(value.columns))], generate_metadata=False)
        else:
            if len(value) != 1:
                if is_argument:
                    raise exceptions.InvalidPipelineError(
                        "Argument '{argument_name}' of step {step_index} of pipeline '{pipeline_id}' is singleton data, but available data is not.".format(
                            argument_name=name,
                            step_index=self.current_step,
                            pipeline_id=self.pipeline.id,
                        ),
                    )
                else:
                    raise exceptions.InvalidPipelineError(
                        "Hyper-parameter '{hyperparameter_name}' of step {step_index} of pipeline '{pipeline_id}' is singleton data, but available data is not.".format(
                            hyperparameter_name=name,
                            step_index=self.current_step,
                            pipeline_id=self.pipeline.id,
                        ),
                    )

            singleton_value = value[0]

        if isinstance(singleton_value, types.Container):
            # TODO: Copy metadata from "value" to "singleton_value" as well.
            singleton_value.metadata = value.metadata.set_for_value(singleton_value, generate_metadata=True)

        return singleton_value

    def _prepare_primitive_arguments(self, step: pipeline_module.PrimitiveStep) -> typing.Dict[str, typing.Any]:
        arguments = {}
        for argument_name, argument_description in step.arguments.items():
            argument_value = self.environment[argument_description['data']]

            if argument_description['type'] == pipeline_module.ArgumentType.DATA:
                # We have to extract a singleton value out.
                argument_value = self._get_singleton_value(argument_value, True, argument_name)

            elif argument_description['type'] == pipeline_module.ArgumentType.CONTAINER:
                # Nothing to do.
                pass

            else:
                assert False, argument_description['type']

            arguments[argument_name] = argument_value

        return arguments

    # TODO: Hyper-parameters with primitives as values should always have an instance as the value. Make sure it is so and also that it is copied.
    def _prepare_primitive_hyperparams(self, step: pipeline_module.PrimitiveStep) -> hyperparams_module.Hyperparams:
        pipeline_hyperparams = {}
        for hyperparameter_name, hyperparameter_description in step.hyperparams.items():
            if hyperparameter_description['type'] == pipeline_module.ArgumentType.DATA:
                if isinstance(hyperparameter_description['data'], typing.Sequence):
                    pipeline_hyperparams[hyperparameter_name] = [
                        self._get_singleton_value(self.environment[data_reference], False, hyperparameter_name)
                        for data_reference in hyperparameter_description['data']
                    ]
                else:
                    pipeline_hyperparams[hyperparameter_name] = self._get_singleton_value(self.environment[hyperparameter_description['data']], False, hyperparameter_name)

            elif hyperparameter_description['type'] == pipeline_module.ArgumentType.PRIMITIVE:
                if isinstance(hyperparameter_description['data'], typing.Sequence):
                    primitive_references = hyperparameter_description['data']
                else:
                    primitive_references = typing.cast(typing.Sequence, [hyperparameter_description['data']])

                primitives = []
                for primitive_reference in primitive_references:
                    primitive = self.steps_state[primitive_reference]

                    if not isinstance(primitive, base.PrimitiveBase):
                        raise exceptions.InvalidPipelineError(
                            "Hyper-parameter '{hyperparameter_name}' of step {step_index} of pipeline '{pipeline_id}' does not point to a primitive step (step {primitive_reference}).".format(  # noqa
                                hyperparameter_name=hyperparameter_name,
                                step_index=self.current_step,
                                pipeline_id=self.pipeline.id,
                                primitive_reference=primitive_reference,
                            ),
                        )

                    primitives.append(primitive)

                if isinstance(hyperparameter_description['data'], typing.Sequence):
                    pipeline_hyperparams[hyperparameter_name] = primitives
                else:
                    assert len(primitives) == 1

                    pipeline_hyperparams[hyperparameter_name] = primitives[0]  # type: ignore

            elif hyperparameter_description['type'] == pipeline_module.ArgumentType.CONTAINER:
                pipeline_hyperparams[hyperparameter_name] = self.environment[hyperparameter_description['data']]

            elif hyperparameter_description['type'] == pipeline_module.ArgumentType.VALUE:
                pipeline_hyperparams[hyperparameter_name] = hyperparameter_description['data']

            else:
                assert False, hyperparameter_description['type']

        if self.hyperparams is not None:
            runtime_hyperparams = self.hyperparams[self.current_step]

            # We checked this already in "_check_hyperparams".
            assert isinstance(runtime_hyperparams, (dict, frozendict.frozendict)), runtime_hyperparams
        else:
            runtime_hyperparams = {}

        hyperparams_class = step.get_primitive_hyperparams()

        # Runtime hyper-parameters should not override pipeline hyper-parameters.
        # We check this is "_check_hyperparams" call from the constructor.
        return hyperparams_class.defaults().replace(pipeline_hyperparams).replace(runtime_hyperparams)

    def _filter_arguments(self, primitive_class: typing.Type[base.PrimitiveBase], method_name: str, arguments: typing.Dict[str, typing.Any]) -> typing.Dict[str, typing.Any]:
        """
        Primitive as a whole gets arguments for all its methods, so here we then filter out
        only those arguments expected by a given method.
        """

        method_arguments = primitive_class.metadata.query()['primitive_code'].get('instance_methods', {}).get(method_name, {}).get('arguments', [])

        filtered_arguments = {}
        for argument_name in method_arguments:
            if argument_name in arguments:
                filtered_arguments[argument_name] = arguments[argument_name]

        return filtered_arguments

    def _create_primitive(self, primitive_class: typing.Type[base.PrimitiveBase], hyperparams: hyperparams_module.Hyperparams) -> base.PrimitiveBase:
        """
        Crates an instance of the primitive.
        """

        installation = primitive_class.metadata.query().get('installation', [])

        if self.volumes_dir is not None:
            volumes = {
                entry['key']: os.path.join(self.volumes_dir, entry['digest'])
                for entry in installation if entry.get('key', None) and entry.get('digest', None) and entry.get('type', None) in [
                    metadata_base.PrimitiveInstallationType.FILE,
                    metadata_base.PrimitiveInstallationType.TGZ,
                ]
            }
        else:
            volumes = {}

        constructor_arguments = {
            'hyperparams': hyperparams,
            # We change the random seed in a deterministic way so that it does not matter in which order we run steps.
            'random_seed': self.random_seed + self.current_step,
            'volumes': volumes,
        }

        filtered_arguments = self._filter_arguments(primitive_class, '__init__', constructor_arguments)

        if 'volumes' in filtered_arguments and self.volumes_dir is None:
            raise exceptions.InvalidArgumentValueError("Primitive '{primitive_id}' of step {step_index} of pipeline '{pipeline_id}' requires static files (volumes) but they are not available.".format(
                primitive_id=primitive_class.metadata.query()['id'],
                step_index=self.current_step,
                pipeline_id=self.pipeline.id,
            ))

        return primitive_class(**filtered_arguments)

    def _run_primitive(self, step: pipeline_module.PrimitiveStep) -> None:
        if step.primitive is None:
            raise exceptions.InvalidPipelineError("Primitive has not been resolved.")

        arguments = self._prepare_primitive_arguments(step)

        if self.phase == Phase.FIT:
            hyperparams = self._prepare_primitive_hyperparams(step)

            primitive = self._create_primitive(step.primitive, hyperparams)

            assert self.steps_state[self.current_step] is None
            self.steps_state[self.current_step] = primitive

        else:
            primitive = typing.cast(base.PrimitiveBase, self.steps_state[self.current_step])

            assert isinstance(primitive, base.PrimitiveBase), type(primitive)

        multi_produce_arguments = self._filter_arguments(step.primitive, 'multi_produce', dict(arguments, produce_methods=step.outputs))

        if self.phase == Phase.FIT:
            primitive.set_training_data(**self._filter_arguments(step.primitive, 'set_training_data', arguments))

            # We fit until fitting finishes.
            # TODO: Support configuring limits on iterations/time.
            while True:
                call_result = primitive.fit()
                # Some primitives return "None" instead of "CallResult".
                # TODO: Do not allow "None" with the release of the core package after v2018.7.10.
                if call_result is None or call_result.has_finished:
                    break

            # We produce until producing finishes.
            # TODO: Produce again only those produce methods which have not finished, currently we simply run all of them again.
            while True:
                multi_call_result = primitive.multi_produce(**multi_produce_arguments)
                if multi_call_result.has_finished:
                    outputs = multi_call_result.values
                    break

        elif self.phase == Phase.PRODUCE:
            # We produce until producing finishes.
            # TODO: Produce again only those produce methods which have not finished, currently we simply run all of them again.
            while True:
                multi_call_result = primitive.multi_produce(**multi_produce_arguments)
                if multi_call_result.has_finished:
                    outputs = multi_call_result.values
                    break

        else:
            assert False, self.phase

        for output_id in step.outputs:
            output_data_reference = 'steps.{i}.{output_id}'.format(i=step.index, output_id=output_id)

            self.environment[output_data_reference] = outputs[output_id]

    def _get_outputs(self) -> typing.Sequence[typing.Any]:
        outputs = []
        for output_description in self.pipeline.outputs:
            outputs.append(self.environment[output_description['data']])

        return outputs

    def _run(self, inputs: typing.Sequence[typing.Any], phase: Phase) -> typing.Sequence[typing.Any]:
        self._check_pipeline(inputs)
        self._initialize_run_state(inputs, phase)

        for step_index, step in enumerate(self.pipeline.steps):
            self.current_step = step_index

            if isinstance(step, pipeline_module.PlaceholderStep):
                self._run_placeholder(step)
            elif isinstance(step, pipeline_module.SubpipelineStep):
                self._run_subpipeline(step)
            elif isinstance(step, pipeline_module.PrimitiveStep):
                self._run_primitive(step)
            else:
                # TODO: Allow dispatch to a general method so that subclasses of this class can handle them if necessary.
                assert False, type(step)

        outputs = self._get_outputs()

        self._delete_run_state()

        return outputs

    def fit(self, inputs: typing.Sequence[typing.Any]) -> typing.Sequence[typing.Any]:
        """
        Does a "fit" run of the pipeline.

        Returns outputs produced during the "fit" run.
        """

        return self._run(inputs, Phase.FIT)

    def produce(self, inputs: typing.Sequence[typing.Any]) -> typing.Sequence[typing.Any]:
        """
        Does a "produce" run of the pipeline and returns outputs.
        """

        return self._run(inputs, Phase.PRODUCE)

    # TODO: Warn if targets are in the problem description, but they have not been applied.
    def _mark_targets(self, dataset: container.Dataset) -> container.Dataset:
        if self.problem_description is None:
            return dataset

        dataset = dataset.copy()
        dataset_id = dataset.metadata.query(())['id']

        for problem_input in self.problem_description.get('inputs', []):
            # TODO: This is currently too strict because we have only one problem description for both train and test data.
            # if problem_input['dataset_id'] != dataset_id:
            #     continue

            for target in problem_input.get('targets', []):
                dataset.metadata = dataset.metadata.add_semantic_type(
                    (target['resource_id'], metadata_base.ALL_ELEMENTS, target['column_index']),
                    'https://metadata.datadrivendiscovery.org/types/Target',
                )
                dataset.metadata = dataset.metadata.add_semantic_type(
                    (target['resource_id'], metadata_base.ALL_ELEMENTS, target['column_index']),
                    'https://metadata.datadrivendiscovery.org/types/TrueTarget',
                )

        return dataset


# TODO: Warn if not all hyper-parameters from "hyperparameter_values" could be found among free hyper-parameters.
def _prepare_hyperparams(free_hyperparams: typing.Sequence, hyperparameter_values: typing.Dict) -> typing.Sequence:
    """
    Values in ``hyperparameter_values`` should be serialized as JSON, as obtained by JSON-serializing
    the output of hyper-parameter's ``value_to_json`` method call.
    """

    hyperparams: typing.List[typing.Union[typing.Dict, typing.Sequence]] = []

    for free_hyperparams_for_step in free_hyperparams:
        if isinstance(free_hyperparams_for_step, (dict, frozendict.frozendict)):
            values = {}
            for name, hyperparameter in free_hyperparams_for_step.items():
                if name in hyperparameter_values:
                    values[name] = hyperparameter.value_from_json(json.loads(hyperparameter_values[name]))
            hyperparams.append(values)
        elif isinstance(free_hyperparams_for_step, typing.Sequence):
            hyperparams.append(_prepare_hyperparams(free_hyperparams_for_step, hyperparameter_values))
        else:
            assert False, type(free_hyperparams_for_step)

    return hyperparams


# TODO: Add debug logging.
def fit(pipeline: pipeline_module.Pipeline, problem_description: typing.Dict, inputs: typing.Sequence[container.Dataset], *,
        random_seed: int = 0, volumes_dir: str = None) -> typing.Tuple[Runtime, container.DataFrame]:
    for input in inputs:
        if not isinstance(input, container.Dataset):
            raise TypeError("A standard pipeline's input should be of a container Dataset type, not {input_type}.".format(
                input_type=type(input),
            ))

    runtime = Runtime(pipeline, problem_description=problem_description, random_seed=random_seed, volumes_dir=volumes_dir, is_standard_pipeline=True)

    outputs = runtime.fit(inputs)

    if len(outputs) != 1:
        raise ValueError("A standard pipeline should have exactly one output, not {outputs}.".format(
            outputs=len(outputs),
        ))
    if not isinstance(outputs[0], container.DataFrame):
        raise TypeError("A standard pipeline's output should be of a container DataFrame type, not {output_type}.".format(
            output_type=type(outputs[0]),
        ))

    return runtime, outputs[0]


# TODO: Add debug logging.
def produce(fitted_pipeline: Runtime, test_inputs: typing.Sequence[container.Dataset]) -> container.DataFrame:
    for test_input in test_inputs:
        if not isinstance(test_input, container.Dataset):
            raise TypeError("A standard pipeline's input should be of a container Dataset type, not {input_type}.".format(
                input_type=type(test_input),
            ))

    outputs = fitted_pipeline.produce(test_inputs)

    if len(outputs) != 1:
        raise ValueError("A standard pipeline should have exactly one output, not {outputs}.".format(
            outputs=len(outputs),
        ))
    if not isinstance(outputs[0], container.DataFrame):
        raise TypeError("A standard pipeline's output should be of a container DataFrame type, not {output_type}.".format(
            output_type=type(outputs[0]),
        ))

    return outputs[0]


# TODO: Add debug logging.
def score(scoring_pipeline: pipeline_module.Pipeline, predictions: container.DataFrame,
          score_inputs: typing.Sequence[container.Dataset], metrics: typing.Sequence[typing.Dict], *,
          random_seed: int = 0, volumes_dir: str = None) -> container.DataFrame:
    for score_input in score_inputs:
        if not isinstance(score_input, container.Dataset):
            raise TypeError("A scoring pipeline's input should be of a container Dataset type, not {input_type}.".format(
                input_type=type(score_input),
            ))

    if not metrics:
        raise exceptions.InvalidArgumentValueError("A list of metrics for scores to compute cannot be empty.")

    metrics_hyperparameter = []
    for metric in metrics:
        metric_hyperparameter = {'metric': metric['metric'].name, 'k': None, 'pos_label': None}
        metric_hyperparameter.update(metric.get('params', {}))
        metrics_hyperparameter.append(metric_hyperparameter)

    hyperparams = _prepare_hyperparams(scoring_pipeline.get_free_hyperparams(), {
        # We have to JSON-serialize it because "_prepare_hyperparams" expects
        # all values to be JSON-serialized.
        'metrics': json.dumps(metrics_hyperparameter),
    })

    runtime = Runtime(scoring_pipeline, hyperparams, random_seed=random_seed, volumes_dir=volumes_dir)

    inputs = [predictions] + list(score_inputs)  # type: ignore

    # Fit + produce on same data.
    outputs = runtime.fit(inputs)

    if len(outputs) != 1:
        raise ValueError("A scoring pipeline should have exactly one output, not {outputs}.".format(
            outputs=len(outputs),
        ))
    if not isinstance(outputs[0], container.DataFrame):
        raise TypeError("A scoring pipeline's output should be of a container DataFrame type, not {output_type}.".format(
            output_type=type(outputs[0]),
        ))

    return outputs[0]


# TODO: Add debug logging.
def evaluate(pipeline: pipeline_module.Pipeline, data_pipeline: pipeline_module.Pipeline, scoring_pipeline: pipeline_module.Pipeline,
             problem_description: typing.Dict, inputs: typing.Sequence[container.Dataset], data_params: typing.Dict[str, str],
             metrics: typing.Sequence[typing.Dict], *, random_seed: int = 0, volumes_dir: str = None) -> typing.List[container.DataFrame]:
    """
    Values in ``data_params`` should be serialized as JSON, as obtained by JSON-serializing
    the output of hyper-parameter's ``value_to_json`` method call.
    """

    for input in inputs:
        if not isinstance(input, container.Dataset):
            raise TypeError("A data preparation pipeline's input should be of a container Dataset type, not {input_type}.".format(
                input_type=type(input),
            ))

    if 'number_of_folds' in data_params:
        number_of_folds = int(data_params['number_of_folds'])
    else:
        # For now we assume other data preparation pipelines do only one fold. We should standardize
        # more hyper-parameters to gather how many folds have to be made (and not really folds, but
        # more how many input indices have to be passed to the pipeline).
        number_of_folds = 1

    hyperparams = _prepare_hyperparams(data_pipeline.get_free_hyperparams(), data_params)

    runtime = Runtime(data_pipeline, hyperparams, problem_description=problem_description, random_seed=random_seed, volumes_dir=volumes_dir)

    # Fit + produce on same data. The inputs are the list of indicies of folds
    # to generate and a dataset to split.
    outputs = runtime.fit([container.List(range(number_of_folds))] + list(inputs))  # type: ignore

    if len(outputs) != 3:
        raise ValueError("A data preparation pipeline should have exactly three outputs, not {outputs}.".format(
            outputs=len(outputs),
        ))

    for output in outputs:
        if not isinstance(output, container.List):
            raise TypeError("A data preparation pipeline's output should be of a container List type, not {input_type}.".format(
                input_type=type(output),
            ))
        if len(output) != number_of_folds:
            raise ValueError("A data preparation pipeline's output should contain {number_of_folds} datasets, not {length}.".format(
                number_of_folds=number_of_folds,
                length=len(output),
            ))

    scores_list = []
    for train_inputs, test_inputs, score_inputs in zip(*outputs):
        fitted_pipeline, predictions = fit(pipeline, problem_description, [train_inputs], random_seed=random_seed, volumes_dir=volumes_dir)
        predictions = produce(fitted_pipeline, [test_inputs])
        scores = score(scoring_pipeline, predictions, [score_inputs], metrics, random_seed=random_seed, volumes_dir=volumes_dir)
        scores_list.append(scores)

    return scores_list


def get_pipeline(pipeline_path: str, *, strict_resolving: bool = False, pipeline_search_paths: typing.Sequence[str] = None,
                 resolver_class: typing.Type[pipeline_module.Resolver] = pipeline_module.Resolver,
                 pipeline_class: typing.Type[pipeline_module.Pipeline] = pipeline_module.Pipeline) -> pipeline_module.Pipeline:
    resolver = resolver_class(strict_resolving=strict_resolving, pipeline_search_paths=pipeline_search_paths)

    if os.path.exists(pipeline_path):
        with open(pipeline_path, 'r') as pipeline_file:
            if pipeline_path.endswith('.yml'):
                return pipeline_class.from_yaml(pipeline_file, resolver=resolver)
            elif pipeline_path.endswith('.json'):
                return pipeline_class.from_json(pipeline_file, resolver=resolver)
            else:
                raise ValueError("Unknown file extension.")
    else:
        return resolver.get_pipeline(pipeline_path)


def is_uri(uri: str) -> bool:
    try:
        parsed_uri = url_parse.urlparse(uri)
    except Exception:
        return False

    return parsed_uri.scheme != ''


def get_dataset(dataset_uri: str) -> container.Dataset:
    if not is_uri(dataset_uri):
        dataset_uri = 'file://{dataset_doc_path}'.format(dataset_doc_path=os.path.abspath(dataset_uri))

    return container.Dataset.load(dataset_uri)


# TODO: Do not traverse the datasets directory every time.
def parse_meta(meta_file: typing.TextIO, datasets_dir: str) -> typing.Dict:
    meta = json.load(meta_file)

    datasets: typing.Dict[str, str] = {}
    problem_descriptions: typing.Dict[str, str] = {}

    for dirpath, dirnames, filenames in os.walk(datasets_dir, followlinks=True):
        if 'datasetDoc.json' in filenames:
            # Do not traverse further (to not parse "datasetDoc.json" or "problemDoc.json" if they
            # exists in raw data filename).
            dirnames[:] = []

            dataset_path = os.path.join(os.path.abspath(dirpath), 'datasetDoc.json')

            try:
                with open(dataset_path, 'r') as dataset_file:
                    dataset_doc = json.load(dataset_file)

                dataset_id = dataset_doc['about']['datasetID']

                if dataset_id in datasets:
                    logger.warning(
                        "Duplicate dataset ID '%(dataset_id)s': '%(old_dataset)s' and '%(dataset)s'", {
                            'dataset_id': dataset_id,
                            'dataset': dataset_path,
                            'old_dataset': datasets[dataset_id],
                        },
                    )
                else:
                    datasets[dataset_id] = dataset_path

            except (ValueError, KeyError):
                logger.exception(
                    "Unable to read dataset '%(dataset)s'.", {
                        'dataset': dataset_path,
                    },
                )

        if 'problemDoc.json' in filenames:
            # We continue traversing further in this case.

            problem_path = os.path.join(os.path.abspath(dirpath), 'problemDoc.json')

            try:
                with open(problem_path, 'r') as problem_file:
                    problem_doc = json.load(problem_file)

                problem_id = problem_doc['about']['problemID']

                if problem_id in problem_descriptions:
                    logger.warning(
                        "Duplicate problem ID '%(problem_id)s': '%(old_problem)s' and '%(problem)s'", {
                            'problem_id': problem_id,
                            'problem': problem_path,
                            'old_problem': problem_descriptions[problem_id],
                        },
                    )
                else:
                    problem_descriptions[problem_id] = problem_path

            except (ValueError, KeyError):
                logger.exception(
                    "Unable to read problem description '%(problem)s'.", {
                        'problem': problem_path,
                    },
                )

    return {
        'problem': problem.parse_problem_description(problem_descriptions[meta['problem']]),
        'train_inputs': [get_dataset(datasets[input_id]) for input_id in meta['train_inputs']],
        'test_inputs': [get_dataset(datasets[input_id]) for input_id in meta['test_inputs']],
        'score_inputs': [get_dataset(datasets[input_id]) for input_id in meta['score_inputs']],
    }


def export_dataframe(dataframe: container.DataFrame, output_file: typing.TextIO) -> None:
    column_names = []
    for column_index in range(len(dataframe.columns)):
        # We use column name from the DataFrame is metadata does not have it. This allows a bit more compatibility.
        column_names.append(dataframe.metadata.query_column(column_index).get('name', dataframe.columns[column_index]))

    dataframe.to_csv(output_file, header=column_names, index=False)


def get_metrics_from_list(metrics: typing.Sequence[str]) -> typing.Sequence[typing.Dict]:
    return [{'metric': problem.PerformanceMetric[metric]} for metric in metrics]


def get_metrics_from_problem_description(problem_description: typing.Dict) -> typing.Sequence[typing.Dict]:
    return problem_description['problem'].get('performance_metrics', [])


def _fit(arguments: argparse.Namespace) -> None:
    pipeline = get_pipeline(arguments.pipeline, strict_resolving=arguments.strict_resolving, pipeline_search_paths=arguments.pipeline_search_paths)

    if arguments.meta is not None:
        meta = parse_meta(arguments.meta, arguments.datasets_dir)
        problem_description = meta['problem']
        inputs = meta['train_inputs']
    else:
        problem_description = problem.parse_problem_description(arguments.problem)
        inputs = [get_dataset(input_uri) for input_uri in arguments.inputs]

    fitted_pipeline, predictions = fit(pipeline, problem_description, inputs, random_seed=arguments.random_seed, volumes_dir=arguments.volumes_dir)

    if arguments.save is not None:
        pickle.dump(fitted_pipeline, arguments.save)

    if arguments.output is not None:
        export_dataframe(predictions, arguments.output)


def _produce(arguments: argparse.Namespace) -> None:
    fitted_pipeline = pickle.load(arguments.fitted_pipeline)

    if arguments.meta is not None:
        meta = parse_meta(arguments.meta, arguments.datasets_dir)
        test_inputs = meta['test_inputs']
    else:
        test_inputs = [get_dataset(input_uri) for input_uri in arguments.test_inputs]

    predictions = produce(fitted_pipeline, test_inputs)

    if arguments.output is not None:
        export_dataframe(predictions, arguments.output)


def _score(arguments: argparse.Namespace) -> None:
    fitted_pipeline = pickle.load(arguments.fitted_pipeline)
    scoring_pipeline = get_pipeline(arguments.scoring_pipeline, strict_resolving=arguments.strict_resolving, pipeline_search_paths=arguments.pipeline_search_paths)

    if arguments.meta is not None:
        meta = parse_meta(arguments.meta, arguments.datasets_dir)
        test_inputs = meta['test_inputs']
        score_inputs = meta['score_inputs']
    else:
        test_inputs = [get_dataset(input_uri) for input_uri in arguments.test_inputs]
        score_inputs = [get_dataset(score_input_uri) for score_input_uri in arguments.score_inputs]

    if arguments.metrics is not None:
        metrics = get_metrics_from_list(arguments.metrics)
    else:
        metrics = get_metrics_from_problem_description(fitted_pipeline.problem_description)

    predictions = produce(fitted_pipeline, test_inputs)

    if arguments.output is not None:
        export_dataframe(predictions, arguments.output)

    scores = score(scoring_pipeline, predictions, score_inputs, metrics, random_seed=arguments.random_seed, volumes_dir=arguments.volumes_dir)

    if arguments.scores is not None:
        export_dataframe(scores, arguments.scores)


def _fit_produce(arguments: argparse.Namespace) -> None:
    pipeline = get_pipeline(arguments.pipeline, strict_resolving=arguments.strict_resolving, pipeline_search_paths=arguments.pipeline_search_paths)

    if arguments.meta is not None:
        meta = parse_meta(arguments.meta, arguments.datasets_dir)
        problem_description = meta['problem']
        inputs = meta['train_inputs']
        test_inputs = meta['test_inputs']
    else:
        problem_description = problem.parse_problem_description(arguments.problem)
        inputs = [get_dataset(input_uri) for input_uri in arguments.inputs]
        test_inputs = [get_dataset(input_uri) for input_uri in arguments.test_inputs]

    fitted_pipeline, predictions = fit(pipeline, problem_description, inputs, random_seed=arguments.random_seed, volumes_dir=arguments.volumes_dir)

    if arguments.save is not None:
        pickle.dump(fitted_pipeline, arguments.save)

    predictions = produce(fitted_pipeline, test_inputs)

    if arguments.output is not None:
        export_dataframe(predictions, arguments.output)


def _fit_score(arguments: argparse.Namespace) -> None:
    pipeline = get_pipeline(arguments.pipeline, strict_resolving=arguments.strict_resolving, pipeline_search_paths=arguments.pipeline_search_paths)
    scoring_pipeline = get_pipeline(arguments.scoring_pipeline, strict_resolving=arguments.strict_resolving, pipeline_search_paths=arguments.pipeline_search_paths)

    if arguments.meta is not None:
        meta = parse_meta(arguments.meta, arguments.datasets_dir)
        problem_description = meta['problem']
        inputs = meta['train_inputs']
        test_inputs = meta['test_inputs']
        score_inputs = meta['score_inputs']
    else:
        problem_description = problem.parse_problem_description(arguments.problem)
        inputs = [get_dataset(input_uri) for input_uri in arguments.inputs]
        test_inputs = [get_dataset(input_uri) for input_uri in arguments.test_inputs]
        score_inputs = [get_dataset(score_input_uri) for score_input_uri in arguments.score_inputs]

    if arguments.metrics is not None:
        metrics = get_metrics_from_list(arguments.metrics)
    else:
        metrics = get_metrics_from_problem_description(problem_description)

    fitted_pipeline, predictions = fit(pipeline, problem_description, inputs, random_seed=arguments.random_seed, volumes_dir=arguments.volumes_dir)

    if arguments.save is not None:
        pickle.dump(fitted_pipeline, arguments.save)

    predictions = produce(fitted_pipeline, test_inputs)

    if arguments.output is not None:
        export_dataframe(predictions, arguments.output)

    scores = score(scoring_pipeline, predictions, score_inputs, metrics, random_seed=arguments.random_seed, volumes_dir=arguments.volumes_dir)

    if arguments.scores is not None:
        export_dataframe(scores, arguments.scores)


def _evaluate(arguments: argparse.Namespace) -> None:
    pipeline = get_pipeline(arguments.pipeline, strict_resolving=arguments.strict_resolving, pipeline_search_paths=arguments.pipeline_search_paths)
    data_pipeline = get_pipeline(arguments.data_pipeline, strict_resolving=arguments.strict_resolving, pipeline_search_paths=arguments.pipeline_search_paths)
    scoring_pipeline = get_pipeline(arguments.scoring_pipeline, strict_resolving=arguments.strict_resolving, pipeline_search_paths=arguments.pipeline_search_paths)

    if arguments.meta is not None:
        meta = parse_meta(arguments.meta, arguments.datasets_dir)
        problem_description = meta['problem']
        inputs = meta['train_inputs']
    else:
        problem_description = problem.parse_problem_description(arguments.problem)
        inputs = [get_dataset(input_uri) for input_uri in arguments.inputs]

    if arguments.data_params is not None:
        data_params = {name: value for name, value in arguments.data_params}
    else:
        data_params = {}

    if arguments.metrics is not None:
        metrics = get_metrics_from_list(arguments.metrics)
    else:
        metrics = get_metrics_from_problem_description(problem_description)

    scores_list = evaluate(pipeline, data_pipeline, scoring_pipeline, problem_description, inputs, data_params, metrics, random_seed=arguments.random_seed, volumes_dir=arguments.volumes_dir)

    assert len(scores_list) > 0

    # We combine multiple scores tables into one output table by adding a "fold" column.
    for fold, scores in enumerate(scores_list):
        fold_column = container.DataFrame({'fold': [fold] * scores.shape[0]})
        # We add the new column at the end so that we do not have to do complicated
        # changes to the metadata.
        scores_list[fold] = pandas.concat([scores, fold_column], axis=1)
        # There is one more column now, so we update metadata for it.
        scores_list[fold].metadata = scores.metadata.update((metadata_base.ALL_ELEMENTS,), {
            'dimension': {
                'length': scores_list[fold].shape[1],
            },
        }, for_value=scores_list[fold])
        scores_list[fold].metadata = scores_list[fold].metadata.update_column(scores_list[fold].shape[1] - 1, {
            'name': 'fold',
            'structural_type': int,
        })

    scores = pandas.concat(scores_list, axis=0).reset_index(drop=True)
    # We reuse metadata from the first fold and update the number of rows which is now
    # combined across all folds.
    scores.metadata = scores_list[0].metadata.update((), {
        'dimension': {
            'length': scores.shape[0],
        },
    }, for_value=scores)

    if arguments.scores is not None:
        export_dataframe(scores, arguments.scores)


def main() -> None:
    logging.basicConfig()

    parser = argparse.ArgumentParser(description="Run D3M pipelines with default hyper-parameters.")

    # TODO: Add "-l" argument for file path where to store "pipeline run" output.
    parser.add_argument(
        '-n', '--random-seed', type=int, default=0, action='store', metavar='SEED',
        help="random seed to use",
    )
    parser.add_argument(
        '-r', '--strict-resolving', default=False, action='store_true',
        help="fail resolving if a resolved primitive does not fully match specified primitive reference",
    )
    parser.add_argument(
        '-p', '--pipelines-path', action='append', metavar='PATH', dest='pipeline_search_paths',
        help="path to a directory with pipelines to resolve from (<pipeline id>.json and <pipeline id>.yml), can be specified multiple times, has priority over PIPELINES_PATH environment variable",
    )
    parser.add_argument(
        '-v', '--volumes', action='store', dest='volumes_dir',
        help="path to a directory with static files required by primitives, in the standard directory structure (as obtained running \"python3 -m d3m.index download\")",
    )
    parser.add_argument(
        '-d', '--datasets', action='store', dest='datasets_dir',
        help="path to a directory with datasets (and problem descriptions) to resolve IDs in meta files",
    )

    subparsers = parser.add_subparsers(dest='command', title='commands')
    subparsers.required = True  # type: ignore

    # TODO: Add command to compute "can_accept" over the pipeline.
    fit_parser = subparsers.add_parser(
        'fit', help="fit a pipeline",
        description="Fit a pipeline on train data.",
    )
    produce_parser = subparsers.add_parser(
        'produce', help="produce using a fitted pipeline",
        description="Produce predictions on test data.",
    )
    score_parser = subparsers.add_parser(
        'score', help="produce using a fitted pipeline and score results",
        description="Produce predictions on test data and compute scores.",
    )
    fit_produce_parser = subparsers.add_parser(
        'fit-produce', help="fit a pipeline and then produce using it",
        description="Fit a pipeline and produce predictions.",
    )
    fit_score_parser = subparsers.add_parser(
        'fit-score', help="fit a pipeline, produce using it and score results",
        description="Fit a pipeline, then produce predictions and compute scores.",
    )
    evaluate_parser = subparsers.add_parser(
        'evaluate', help="evaluate a pipeline",
        description="Run pipeline multiple times using an evaluation approach and compute scores for each run.",
    )

    fit_parser.add_argument(
        '-p', '--pipeline', action='store', required=True,
        help="path to a pipeline file (.json or .yml) or pipeline ID",
    )
    fit_parser.add_argument(
        '-r', '--problem', action='store',
        help="path to a problem description file",
    )
    fit_parser.add_argument(
        '-i', '--input', action='append', metavar='INPUT', dest='inputs',
        help="path or URI of an input train dataset",
    )
    fit_parser.add_argument(
        '-m', '--meta', type=argparse.FileType('r', encoding='utf8'), action='store',
        help="path to a meta file with configuration",
    )
    fit_parser.add_argument(
        '-s', '--save', type=argparse.FileType('wb'), action='store',
        help="save fitted pipeline to a file",
    )
    fit_parser.add_argument(
        '-o', '--output', type=argparse.FileType('w', encoding='utf8'), default='-', action='store',
        help="save produced predictions during fitting to a file, default stdout",
    )
    fit_parser.set_defaults(handler=_fit)

    produce_parser.add_argument(
        '-f', '--fitted-pipeline', type=argparse.FileType('rb'), action='store', required=True,
        help="path to a saved fitted pipeline",
    )
    produce_parser.add_argument(
        '-t', '--test-input', action='append', metavar='INPUT', dest='test_inputs',
        help="path or URI of an input test dataset",
    )
    produce_parser.add_argument(
        '-m', '--meta', type=argparse.FileType('r', encoding='utf8'), action='store',
        help="path to a meta file with configuration",
    )
    produce_parser.add_argument(
        '-o', '--output', type=argparse.FileType('w', encoding='utf8'), default='-', action='store',
        help="save produced predictions to a file, default stdout",
    )
    produce_parser.set_defaults(handler=_produce)

    score_parser.add_argument(
        '-f', '--fitted-pipeline', type=argparse.FileType('rb'), action='store', required=True,
        help="path to a saved fitted pipeline",
    )
    score_parser.add_argument(
        '-n', '--scoring-pipeline', action='store', required=True,
        help="path to a scoring pipeline file (.json or .yml) or pipeline ID",
    )
    score_parser.add_argument(
        '-t', '--test-input', action='append', metavar='INPUT', dest='test_inputs',
        help="path or URI of an input test dataset",
    )
    score_parser.add_argument(
        '-a', '--score-input', action='append', metavar='INPUT', dest='score_inputs',
        help="path or URI of an input score dataset",
    )
    score_parser.add_argument(
        '-m', '--meta', type=argparse.FileType('r', encoding='utf8'), action='store',
        help="path to a meta file with configuration",
    )
    score_parser.add_argument(
        '-e', '--metric', choices=[metric.name for metric in problem.PerformanceMetric], action='append', metavar='METRIC', dest='metrics',
        help="metric to use, using default parameters, can be specified multiple times, default from problem description",
    )
    score_parser.add_argument(
        '-o', '--output', type=argparse.FileType('w', encoding='utf8'), action='store',
        help="save produced predictions to a file",
    )
    score_parser.add_argument(
        '-c', '--scores', type=argparse.FileType('w', encoding='utf8'), default='-', action='store',
        help="save scores to a file, default stdout",
    )
    score_parser.set_defaults(handler=_score)

    fit_produce_parser.add_argument(
        '-p', '--pipeline', action='store', required=True,
        help="path to a pipeline file (.json or .yml) or pipeline ID",
    )
    fit_produce_parser.add_argument(
        '-r', '--problem', action='store',
        help="path to a problem description file",
    )
    fit_produce_parser.add_argument(
        '-i', '--input', action='append', metavar='INPUT', dest='inputs',
        help="path or URI of an input train dataset",
    )
    fit_produce_parser.add_argument(
        '-t', '--test-input', action='append', metavar='INPUT', dest='test_inputs',
        help="path or URI of an input test dataset",
    )
    fit_produce_parser.add_argument(
        '-m', '--meta', type=argparse.FileType('r', encoding='utf8'), action='store',
        help="path to a meta file with configuration",
    )
    fit_produce_parser.add_argument(
        '-s', '--save', type=argparse.FileType('wb'), action='store',
        help="save fitted pipeline to a file",
    )
    fit_produce_parser.add_argument(
        '-o', '--output', type=argparse.FileType('w', encoding='utf8'), default='-', action='store',
        help="save produced predictions to a file, default stdout",
    )
    fit_produce_parser.set_defaults(handler=_fit_produce)

    fit_score_parser.add_argument(
        '-p', '--pipeline', action='store', required=True,
        help="path to a pipeline file (.json or .yml) or pipeline ID",
    )
    fit_score_parser.add_argument(
        '-n', '--scoring-pipeline', action='store', required=True,
        help="path to a scoring pipeline file (.json or .yml) or pipeline ID",
    )
    fit_score_parser.add_argument(
        '-r', '--problem', action='store',
        help="path to a problem description file",
    )
    fit_score_parser.add_argument(
        '-i', '--input', action='append', metavar='INPUT', dest='inputs',
        help="path or URI of an input train dataset",
    )
    fit_score_parser.add_argument(
        '-t', '--test-input', action='append', metavar='INPUT', dest='test_inputs',
        help="path or URI of an input test dataset",
    )
    fit_score_parser.add_argument(
        '-a', '--score-input', action='append', metavar='INPUT', dest='score_inputs',
        help="path or URI of an input score dataset",
    )
    fit_score_parser.add_argument(
        '-m', '--meta', type=argparse.FileType('r', encoding='utf8'), action='store',
        help="path to a meta file with configuration",
    )
    fit_score_parser.add_argument(
        '-e', '--metric', choices=[metric.name for metric in problem.PerformanceMetric], action='append', metavar='METRIC', dest='metrics',
        help="metric to use, using default parameters, can be specified multiple times, default from problem description",
    )
    fit_score_parser.add_argument(
        '-s', '--save', type=argparse.FileType('wb'), action='store',
        help="save fitted pipeline to a file",
    )
    fit_score_parser.add_argument(
        '-o', '--output', type=argparse.FileType('w', encoding='utf8'), action='store',
        help="save produced predictions to a file",
    )
    fit_score_parser.add_argument(
        '-c', '--scores', type=argparse.FileType('w', encoding='utf8'), default='-', action='store',
        help="save scores to a file, default stdout",
    )
    fit_score_parser.set_defaults(handler=_fit_score)

    evaluate_parser.add_argument(
        '-p', '--pipeline', action='store', required=True,
        help="path to a pipeline file (.json or .yml) or pipeline ID"
    )
    evaluate_parser.add_argument(
        '-d', '--data-pipeline', action='store', required=True,
        help="path to a data preparation pipeline file (.json or .yml) or pipeline ID",
    )
    evaluate_parser.add_argument(
        '-n', '--scoring-pipeline', action='store', required=True,
        help="path to a scoring pipeline file (.json or .yml) or pipeline ID",
    )
    evaluate_parser.add_argument(
        '-r', '--problem', action='store',
        help="path to a problem description file",
    )
    evaluate_parser.add_argument(
        '-i', '--input', action='append', metavar='INPUT', dest='inputs',
        help="path or URI of an input full dataset",
    )
    evaluate_parser.add_argument(
        '-m', '--meta', type=argparse.FileType('r', encoding='utf8'), action='store',
        help="path to a meta file with configuration",
    )
    evaluate_parser.add_argument(
        '-y', '--data-param', nargs=2, action='append', metavar=('NAME', 'VALUE'), dest='data_params',
        help="hyper-parameter name and its value for data pipeline, can be specified multiple times, value should be JSON-serialized",
    )
    evaluate_parser.add_argument(
        '-e', '--metric', choices=[metric.name for metric in problem.PerformanceMetric], action='append', metavar='METRIC', dest='metrics',
        help="metric to use, using default parameters, can be specified multiple times, default from problem description",
    )
    evaluate_parser.add_argument(
        '-c', '--scores', type=argparse.FileType('w', encoding='utf8'), default='-', action='store',
        help="save scores to a file, default stdout",
    )
    evaluate_parser.set_defaults(handler=_evaluate)

    arguments = parser.parse_args()

    # Dynamically fetch which subparser was used.
    subparser = parser._subparsers._group_actions[0].choices[arguments.command]  # type: ignore

    # TODO: These arguments are required, but this is not visible from the usage line. These arguments are marked as optional there.
    manual_config = [('-r/--problem', 'problem'), ('-i/--input', 'inputs'), ('-t/--test-input', 'test_inputs'), ('-a/--score-input', 'score_inputs')]
    if any(hasattr(arguments, dest) and getattr(arguments, dest) is not None for (name, dest) in manual_config) and arguments.meta is not None:
        subparser.error("the following arguments cannot be used together: {manual_arguments} and -m/--meta".format(
            manual_arguments=', '.join(name for (name, dest) in manual_config if hasattr(arguments, dest) and getattr(arguments, dest) is not None),
        ))
    elif any(hasattr(arguments, dest) and getattr(arguments, dest) is None for (name, dest) in manual_config) and arguments.meta is None:
        subparser.error("the following arguments are required: {manual_arguments} or -m/--meta".format(
           manual_arguments=', '.join(name for (name, dest) in manual_config if hasattr(arguments, dest)),
        ))

    # Call a handler for the command.
    arguments.handler(arguments)


if __name__ == '__main__':
    main()
